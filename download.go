package sshexplorer

import (
	"context"
	"io"
	"os"
	"path/filepath"

	"github.com/pkg/errors"
)

func (c *Client) Download(ctx context.Context, remotePath, localPath string, localFileMode os.FileMode) error {
	remoteFile, err := c.sftpClient.Open(remotePath)
	if err != nil {
		return errors.Wrap(err, "failed to open remote file")
	}
	defer remoteFile.Close()

	if err = os.MkdirAll(filepath.Dir(localPath), localFileMode); err != nil {
		return errors.Wrapf(err, "failed to create directory %q", filepath.Dir(localPath))
	}

	tempLocalPath := localPath + ".part"

	tempLocalFile, err := os.Create(tempLocalPath)
	if err != nil {
		return errors.Wrap(err, "failed to create temp local file")
	}
	defer tempLocalFile.Close()

	if _, err = io.Copy(tempLocalFile, remoteFile); err != nil {
		return errors.Wrap(err, "failed to copy remote file")
	}

	if err = tempLocalFile.Sync(); err != nil {
		return errors.Wrap(err, "failed to sync temp local file")
	}

	if err = tempLocalFile.Chmod(localFileMode); err != nil {
		return errors.Wrap(err, "failed to chmod temp local file")
	}

	if err = os.Rename(tempLocalPath, localPath); err != nil {
		return errors.Wrap(err, "failed to rename temp local file")
	}

	return nil
}
