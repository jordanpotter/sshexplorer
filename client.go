package sshexplorer

import (
	"github.com/pkg/errors"
	"github.com/pkg/sftp"
	"golang.org/x/crypto/ssh"
)

type Client struct {
	sshClient  *ssh.Client
	sftpClient *sftp.Client
}

func New(address, username, password string) (*Client, error) {
	sshConfig := ssh.ClientConfig{
		User: username,
		Auth: []ssh.AuthMethod{
			ssh.Password(password),
		},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	}

	sshClient, err := ssh.Dial("tcp", address, &sshConfig)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to dial %q", address)
	}

	sftpClient, err := sftp.NewClient(sshClient)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create SFTP client")
	}

	return &Client{
		sshClient:  sshClient,
		sftpClient: sftpClient,
	}, nil
}

func (c *Client) Close() error {
	if err := c.sshClient.Close(); err != nil {
		return errors.Wrap(err, "failed to close SSH client")
	}

	if err := c.sftpClient.Close(); err != nil {
		return errors.Wrap(err, "failed to close SFTP client")
	}

	return nil
}
