package sshexplorer

import (
	"context"
	"path/filepath"
	"strings"

	"github.com/pkg/errors"
)

func (c *Client) Files(ctx context.Context, root string, minDepth, maxDepth int) ([]string, error) {
	rootDepth := len(strings.Split(root, string(filepath.Separator)))

	var paths []string

	walker := c.sftpClient.Walk(root)
	for walker.Step() {
		if ctx.Err() != nil {
			return nil, errors.Wrap(ctx.Err(), "failed to walk")
		}

		if err := walker.Err(); err != nil {
			return nil, errors.Wrap(err, "failed to walk")
		}

		depth := len(strings.Split(walker.Path(), string(filepath.Separator)))
		if depth < rootDepth+minDepth {
			continue
		} else if depth > rootDepth+maxDepth {
			if walker.Stat().IsDir() {
				walker.SkipDir()
			}

			continue
		}

		if !walker.Stat().IsDir() {
			paths = append(paths, walker.Path())
		}

		if depth == rootDepth+maxDepth && walker.Stat().IsDir() {
			walker.SkipDir()
		}
	}

	return paths, nil
}
